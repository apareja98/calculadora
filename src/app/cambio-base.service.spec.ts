import { TestBed, inject } from '@angular/core/testing';

import { CambioBaseService } from './cambio-base.service';

describe('CambioBaseService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CambioBaseService]
    });
  });

  it('should be created', inject([CambioBaseService], (service: CambioBaseService) => {
    expect(service).toBeTruthy();
  }));
});
