import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CambioBaseComponent } from './cambio-base.component';

describe('CambioBaseComponent', () => {
  let component: CambioBaseComponent;
  let fixture: ComponentFixture<CambioBaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CambioBaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CambioBaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
