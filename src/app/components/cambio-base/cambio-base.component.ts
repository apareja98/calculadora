import { Component, OnInit } from '@angular/core';
import {NgClass} from '@angular/common';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-cambio-base',
  templateUrl: './cambio-base.component.html',
  styleUrls: ['./cambio-base.component.css']
})
export class CambioBaseComponent implements OnInit {
  //Declaracion de variables, que son las mismas que se llaman en el html

  active:number=0;
  numero:string="";
  valores:any;
  
  constructor(private http: HttpClient) { }

  ngOnInit() {
  }

  seleccionarBase(e){
    if(this.active!=e)this.numero=""
    this.active=e;
    console.log(this.active);
  }

  capturarValor(e){
   if(this.active<=10){
      if(e<this.active){
        this.numero+=e;
      }
    }else{
      if(e==10)this.numero+="A";
      if(e==11)this.numero+="B";
      if(e==12)this.numero+="C";   
      if(e==13)this.numero+="D";
      if(e==14)this.numero+="E";
      if(e==15)this.numero+="F";
     
     
     
    }
    //Se consume el método get desde el servidor 
    this.http.get('http://127.0.0.1:5000/cambioBase/'+ this.numero +'/'+ this.active).subscribe(data =>{
      this.valores = data;
    
    })

  }

  borrar(){
    this.numero="";
  }
  
  esActivo(e){
    return this.active==e;
  }

}
